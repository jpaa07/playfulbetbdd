package com.qvision.playfulbet.Steps;

import com.qvision.playfulbet.Pages.VideosPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
import org.fluentlenium.core.annotation.Page;

/**
 * Created by Lenovo on 22/03/2017.
 */
public class VideosSteps extends ScenarioSteps {
    @Page
    private VideosPage videosPage;

    @Step
    public void goToVideos(){
        videosPage.goToVideos();
    }

    @Step
    public int watchVideo(){
        return videosPage.watchVideo();
    }

    @Step
    public void verifyIfVideoAvailable(){
        videosPage.VerifyIfVideoAvailable();
    }
}
