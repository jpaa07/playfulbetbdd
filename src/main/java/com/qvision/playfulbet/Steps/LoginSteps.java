package com.qvision.playfulbet.Steps;

import com.qvision.playfulbet.Pages.LoginPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
import org.fluentlenium.core.annotation.Page;
import org.jbehave.core.model.ExamplesTable;

/**
 * Created by Lenovo on 13/03/2017.
 */
public class LoginSteps extends ScenarioSteps {
    @Page
    private LoginPage loginPage;

    @Step
    public void openPage(){
        loginPage.open();
    }

    @Step
    public void goToLogin(){
        loginPage.goToLogin();
    }

    @Step
    public void login(ExamplesTable loginData){
        loginPage.login(loginData);
    }

    @Step
    public void validateIfPassed(){
        loginPage.validateIfPassed();
    }
}
