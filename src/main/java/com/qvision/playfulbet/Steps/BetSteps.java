package com.qvision.playfulbet.Steps;

import com.qvision.playfulbet.Pages.BetPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
import org.fluentlenium.core.annotation.Page;
import org.jbehave.core.model.ExamplesTable;

/**
 * Created by Lenovo on 13/03/2017.
 */
public class BetSteps extends ScenarioSteps {
    @Page
    private BetPage betPage;

    @Step
    public void searchTeam(ExamplesTable teamData){
        betPage.searchTeam(teamData);
    }

    @Step
    public void goToEvent(){
        betPage.goToEvent();
    }

    @Step
    public void makeABet(ExamplesTable betData){
        betPage.makeABet(betData);
    }

    @Step
    public void validateBet(){
        betPage.validateBet();
    }
}
