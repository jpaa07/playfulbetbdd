package com.qvision.playfulbet.Pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.hamcrest.MatcherAssert;

/**
 * Created by Lenovo on 22/03/2017.
 */
public class VideosPage extends PageObject{

    @FindBy(className = "im-promos")
    private WebElementFacade btnGoToPromos;

    @FindBy(xpath = "/html[@class=' js no-touch csstransforms csstransforms3d csstransitions svg inlinesvg smil " +
                    "svgclippaths no-ie8compat']/body[@class='body-container']/div[@class='container']/section" +
                    "[@class='wrapper']/section[@class='small-12 wrapper-inner js-spinner-wrapper is-fadeIn']" +
                    "/section[@class='wrapper--content']/div[@class='row container-up-dw']/div[@class='column " +
                    "large-3']/article[@class='box--sml im-promos-sidebar']/div[@class='row']/div[@class='column']" +
                    "/ul[@class='no-bullet flush box-v-list']/li[6]/a")
    private WebElementFacade btnGoToVideos;

    @FindBy(className = "gtm-vidcoin")
    private WebElementFacade btnWatchVideo;

    @FindBy(className = "//*[@id=\"video_container\"]")
    private WebElementFacade frameVideo;

    @FindBy(className = "btn_validate")
    private WebElementFacade claimCoins;

    @FindBy(xpath = "/html[@class=' js no-touch csstransforms csstransforms3d csstransitions svg inlinesvg smil " +
            "svgclippaths no-ie8compat']/body[@class='body-container']/div[@class='container']/section[@class='wrapper'" +
            "]/section[@class='small-12 wrapper-inner js-spinner-wrapper is-fadeIn']/section[@class='wrapper--content']" +
            "/div[@class='row container-up-dw']/div[@class='column large-9']/div[@class='box']/div[@class='row'][1]/div}" +
            "[@class='column medium-6'][4]/article[@class='js-tv-message']")
    private WebElementFacade divNoVideoAvailable;


    public void goToVideos(){
        btnGoToPromos.click();
        btnGoToVideos.waitUntilVisible();
        btnGoToVideos.click();
    }

    public int watchVideo(){
        if (btnWatchVideo.isVisible()){
            btnWatchVideo.click();
            frameVideo.waitUntilVisible();
            frameVideo.click();
            try {
                Thread.sleep(60000);
            } catch (Exception ex){
                System.out.println("error: " + ex);
            }
            claimCoins.click();
            return 0;
        } else {
            return 1;
        }
    }

    public void VerifyIfVideoAvailable(){
        divNoVideoAvailable.waitUntilVisible();
        MatcherAssert.assertThat("No video available",divNoVideoAvailable.isVisible());
    }
}
