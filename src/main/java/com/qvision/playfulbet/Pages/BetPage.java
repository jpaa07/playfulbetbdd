package com.qvision.playfulbet.Pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.hamcrest.MatcherAssert;
import org.jbehave.core.model.ExamplesTable;

import java.util.Map;

/**
 * Created by Lenovo on 13/03/2017.
 */


public class BetPage extends PageObject{

    BusinessRules businessRules;
    static final String TEAMSIDEPATH = "html/body/div[1]/section/section/section[2]/div/div/article[1]/div[1]/div//li";

    @FindBy(className = "dashboard-event")
    private WebElementFacade btnGoToEvent;

    @FindBy(className = "column")
    private WebElementFacade btnTeamSide;

    @FindBy(id = "points")
    private WebElementFacade txtBetCoins;

    @FindBy(id = "play-action")
    private WebElementFacade btnMakeABet;

    @FindBy(className = "flashban")
    private WebElementFacade divBetDone;

    @FindBy(className = "fa-search")
    private WebElementFacade btnSearch;

    @FindBy(id = "nav-search")
    private WebElementFacade txtSearch;

    @FindBy(className = "link-outline")
    private WebElementFacade btnGoToEventSearched;

    @FindBy(xpath = TEAMSIDEPATH + "[1]")
    private WebElementFacade btnBetToHome;

    @FindBy(xpath = TEAMSIDEPATH + "[2]")
    private WebElementFacade btnBetToTie;

    @FindBy(xpath = TEAMSIDEPATH + "[3]")
    private WebElementFacade btnBetToVisit;

    @FindBy(xpath = TEAMSIDEPATH + "[1]//span[2]")
    private WebElementFacade betValueHome;

    @FindBy(xpath = TEAMSIDEPATH + "[2]//span[2]")
    private WebElementFacade betValueTie;

    @FindBy(xpath = TEAMSIDEPATH + "[3]//span[2]")
    private WebElementFacade betValueVisit;

    public void searchTeam(ExamplesTable teamTable){
        Map<String,String> teamData = teamTable.getRow(0);
        btnSearch.waitUntilVisible();
        btnSearch.click();
        txtSearch.waitUntilVisible().typeAndEnter(teamData.get("team"));
        btnGoToEventSearched.waitUntilVisible();
        btnGoToEventSearched.click();
    }

    public void goToEvent(){
        btnGoToEvent.click();
    }

    public void  makeABet(ExamplesTable betTable){
        String homeValue, tieValue, visitValue;
        Map<String,String> betData = betTable.getRow(0);
        btnTeamSide.waitUntilVisible();
        homeValue = betValueHome.getText();
        tieValue = betValueTie.getText();
        visitValue = betValueVisit.getText();
        businessRules.analizeBet(homeValue,tieValue,visitValue);
        btnTeamSide.click();
        txtBetCoins.waitUntilVisible();
        txtBetCoins.clear();
        txtBetCoins.sendKeys(betData.get("coins"));
        btnMakeABet.click();
    }

    public void validateBet(){
        divBetDone.waitUntilVisible();
        MatcherAssert.assertThat("div not visible",divBetDone.isVisible());
    }
}
