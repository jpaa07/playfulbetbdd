package com.qvision.playfulbet.Pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.hamcrest.MatcherAssert;
import org.jbehave.core.model.ExamplesTable;
import org.openqa.selenium.WebDriver;

import java.util.Map;

/**
 * Created by Lenovo on 13/03/2017.
 */
@DefaultUrl("http://playfulbet.com/")
public class LoginPage extends PageObject {
    @FindBy(className = "btn--submit")
    private WebElementFacade btnGoToLogin;

    @FindBy(id = "user_login")
    private WebElementFacade txtUser;

    @FindBy(id = "user_password")
    private WebElementFacade txtPassword;

    @FindBy(name = "button")
    private WebElementFacade btnLogin;

    @FindBy(className = "js-dashboard")
    private WebElementFacade dashboardSection;

    public LoginPage (WebDriver driver) {
        super(driver);
    }

    public void goToLogin(){
        btnGoToLogin.waitUntilVisible();
        btnGoToLogin.click();
    }

    public void login(ExamplesTable loginTable){
        Map<String,String> loginData = loginTable.getRow(0);
        txtUser.waitUntilVisible();
        txtUser.sendKeys(loginData.get("username"));
        txtPassword.sendKeys(loginData.get("password"));
        btnLogin.click();
    }

    public void validateIfPassed(){
        dashboardSection.waitUntilVisible();
        MatcherAssert.assertThat("Dashboard is not visible",dashboardSection.isVisible());
    }
}
