package com.qvision.playfulbet.Definitions;

import com.qvision.playfulbet.Steps.LoginSteps;
import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.model.ExamplesTable;

/**
 * Created by Lenovo on 13/03/2017.
 */
public class LoginDefinitions {
    @Steps
    private LoginSteps loginSteps;

    @Given("I am in the home page of playfulbet")
    public void openPage(){
        loginSteps.openPage();
    }

    @When("I click in the enter and play button")
    public void goToLogin(){
        loginSteps.goToLogin();
    }

    @When("fill user and password fields $loginData")
    public void login(ExamplesTable loginData){
        loginSteps.login(loginData);
    }

    @Then("appears my account dashboard")
    public void validateIfPassed(){
        loginSteps.validateIfPassed();
    }

}
