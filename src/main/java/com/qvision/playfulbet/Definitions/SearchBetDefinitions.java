package com.qvision.playfulbet.Definitions;

import com.qvision.playfulbet.Steps.BetSteps;
import com.qvision.playfulbet.Steps.LoginSteps;
import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.model.ExamplesTable;

/**
 * Created by Lenovo on 13/03/2017.
 */
public class SearchBetDefinitions {
    @Steps
    private BetSteps betSteps;

    @Steps
    private LoginSteps loginSteps;

    @Given("I am in my sccount dashboard search")
    public void validateIfPassed(){
        loginSteps.validateIfPassed();
    }

    @When("I type the team I want to bet in the search box $teamData")
    public void searchTeam(ExamplesTable teamData){
        betSteps.searchTeam(teamData);
    }

    @When("select how many coins bet to the team search $betData")
    public void makeABet(ExamplesTable betData){
        betSteps.makeABet(betData);
    }

    @Then("appears confirmation message search")
    public void validateBet(){
        betSteps.validateBet();
    }
}
