package com.qvision.playfulbet.Definitions;

import com.qvision.playfulbet.Steps.LoginSteps;
import com.qvision.playfulbet.Steps.VideosSteps;
import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

/**
 * Created by Lenovo on 22/03/2017.
 */
public class VideosDefinitions {
    @Steps
    private LoginSteps loginSteps;

    @Steps
    private VideosSteps videosSteps;

    @Given("I am in the home page of playfulbet to videos")
    public void validateIfPassed(){
        loginSteps.validateIfPassed();
    }

    @When("I click in the button go to videos")
    public void goToVideos(){
        videosSteps.goToVideos();
    }

    @When("play video")
    public void watchVideo(){
        int verifyInt = 0;
        while (verifyInt == 0) {
            verifyInt = videosSteps.watchVideo();
        }
    }

    @Then("video ends")
    public void verifyIfVideoAvailable(){
        videosSteps.verifyIfVideoAvailable();
    }
}
